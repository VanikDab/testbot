<?php

namespace app\cron;

require_once 'constants.php';
require_once ROOT . '/classes/facades/Log.php';
require_once ROOT . '/classes/models/History.php';
require_once ROOT . '/classes/commands/CheckDollarCommand.php';

use app\commands\CheckDollarCommand;
use app\models\History;
use app\facades\Log;

/**
 * Class SendMessages
 * @package app\cron
 */
class SendMessages
{
    public const CURL_URL = 'https://api.telegram.org/bot1126136628:AAFvtXWcmdtbDsmilHyaEQvNiw5rfEY8n04/';

    private string $method = 'sendMessage';
    private array $data = [];
    private array $headers = [];
    private History $history;

    /**
     * SendMessages constructor.
     */
    public function __construct()
    {
        $this->history = new History();
    }

    /**
     * Run sending messages
     */
    public function run()
    {
        $users = $this->history->getAllUsersIds();

        $command = new CheckDollarCommand();
        $this->data = $command->getData();

        //Тут надо батчами работать чтобы сервер выдержал
        foreach ($users as $user) {
            $this->sendTo($user['userId']);
        }
    }

    /**
     * @param int $userId
     */
    private function sendTo(int $userId)
    {
        $this->data['chat_id'] = $userId;

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => self::CURL_URL . $this->method,
            CURLOPT_POSTFIELDS => json_encode($this->data),
            CURLOPT_HTTPHEADER => array_merge(array("Content-Type: application/json"), $this->headers)
        ]);

        curl_exec($curl);

        if ($errorMessage = curl_errno($curl)) {
            Log::error([
                'type' => Log::CURL_SENDING_ERROR_CRON,
                'message' => $errorMessage,
                'date' => date('Y-m-d H:i:s')
            ]);
        } else {
            Log::error([
                'type' => Log::TELEGRAM_REQUEST_SUCCESS_CRON,
                'date' => date('Y-m-d H:i:s')
            ]);
        }

        curl_close($curl);
    }
}

$sender = new SendMessages();
$sender->run();