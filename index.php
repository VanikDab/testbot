<?php
// Bot @dev96elbot

require_once 'constants.php';
require_once ROOT . '/classes/controllers/TelegramBot.php';
require_once ROOT . '/classes/models/TelegramRequestObject.php';

use app\controllers\TelegramBot;
use app\models\TelegramRequestObject;

$data = json_decode(file_get_contents('php://input'), TRUE);

// #1190735339:AAGeZVyIHt4ED6z0QInNrTkkWOuxHlzsq5s

$data = $data['callback_query'] ? $data['callback_query'] : $data['message'];

$request = new TelegramRequestObject($data);
$bot = new TelegramBot($request);

$bot->run();