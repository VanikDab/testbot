<?php


namespace app\facades;

require ROOT . '/classes/facades/Log.php';

/**
 * Class DataValidator
 * @package app\facades
 */
class DataValidator
{
    /**
     * @param array $data
     * @return bool
     */
    public static function validate(array $data): bool
    {
        return isset($data['message_id']) && isset($data['date']) && isset($data['text'])
            && isset($data['from']['id']) && isset($data['from']['first_name']) && isset($data['from']['last_name'])
            && isset($data['from']['username']);
    }

}
