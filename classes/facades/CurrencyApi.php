<?php


namespace app\facades;

/**
 * Class CurrencyApi
 * @package app\facades
 */
class CurrencyApi
{
    private const URL = 'https://free.currencyconverterapi.com/api/v5/convert?q=USD_RUB&compact=y&apiKey=afc74e810cf9948ef9ec';

    /**
     * @return string
     */
    public static function getCurrentUSDToRUBValue(): string
    {
        $json = file_get_contents(self::URL);

        $data = json_decode($json, true);

        if(!isset($data['USD_RUB']) || !isset($data['USD_RUB']['val']))
        {
            return 'Попробуйте позже)';
        }

        return $data['USD_RUB']['val'];
    }
}