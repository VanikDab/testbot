<?php


namespace app\facades;


/**
 * Class Log
 */
class Log
{
    public const CURL_SENDING_ERROR = 'curl sending error';
    public const TELEGRAM_REQUEST_ERROR = 'telegram request error';
    public const TELEGRAM_REQUEST_SUCCESS = 'telegram request success';

    public const CURL_SENDING_ERROR_CRON = 'curl sending error cron';
    public const TELEGRAM_REQUEST_SUCCESS_CRON = 'telegram request success cron';

    /**
     * @param array $data
     */
    public static function error(array $data): void
    {
        file_put_contents('error_logs.txt', '$data: ' . print_r($data, 1) . "\n", FILE_APPEND);
    }

    /**
     * @param array $data
     */
    public static function success(array $data): void
    {
        file_put_contents('success_logs.txt', '$data: ' . print_r($data, 1) . "\n", FILE_APPEND);
    }
}