<?php


namespace app\commands;

require_once ROOT . '/classes/commands/Command.php';

/**
 * Class FollowCommand
 * @package app\commands
 */
class FollowCommand extends Command
{
    /**
     * @return array
     */
    public function getData(): array
    {
        return [
            'text' => 'Здраствуйте ' . $this->request->getFirstName() . ". Вы подписались на канал ...",
            'reply_markup' => ['hide_keyboard' => true]
        ];
    }

}