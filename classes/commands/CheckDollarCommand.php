<?php


namespace app\commands;

use app\facades\CurrencyApi;
use app\models\History;

require_once ROOT . '/classes/commands/Command.php';
require_once ROOT . '/classes/facades/CurrencyApi.php';
require_once ROOT . '/classes/models/History.php';

/**
 * Class CheckDollarCommand
 * @package app\commands
 */
class CheckDollarCommand extends Command
{
    /**
     * @return array
     */
    public function getData(): array
    {
        $value = CurrencyApi::getCurrentUSDToRUBValue();

        if($this->request) {
            //В идеале тут должны хранить в редис либо реббит, потом оттуда кроном добавить в mysql
            $history = new History();
            $history
                ->setUserId($this->request->getUserId())
                ->setDate($this->request->getDate())
                ->setCommand($this->request->getCommand())
                ->setUsername($this->request->getUsername())
                ->setFirstName($this->request->getFirstName())
                ->setLastName($this->request->getLastName())
                ->setValue($value)
                ->save();
        }

        return [
            'text' => $value,
            'reply_markup' => ['hide_keyboard' => true]
        ];
    }

}