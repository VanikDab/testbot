<?php


namespace app\commands;

/**
 * Interface CommandInterface
 * @package app\commands
 */
interface CommandInterface
{
    public function getData(): array;
    public function getHeaders(): array;
    public function getMethod(): string;
}