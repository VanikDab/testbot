<?php


namespace app\commands;

use app\models\TelegramRequestObject;

require_once ROOT . '/classes/commands/CommandInterface.php';

/**
 * Class Command
 * @package app\commands
 */
class Command implements CommandInterface
{
    public const CHECK_DOLLAR_COURSE = 'checkdollar';
    public const FOLLOW = 'follow';
    public const HISTORY = 'history';

    protected ?TelegramRequestObject $request = NULL;

    /**
     * Command constructor.
     * @param TelegramRequestObject|null $request
     */
    public function __construct(?TelegramRequestObject $request = NULL)
    {
        if ($request) {
            $this->request = $request;
        }
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $keyboard = [
            'keyboard' => [
                [['text' => 'checkdollar'], ['text' => 'follow'], ['text' => 'history']]
            ],
        ];

        return [
            'text' => 'Пожалуйста выберите команду: 
        	    <pre> checkdollar - Получить курс доллара в рублях </pre>
        	    <pre> follow - Получить курс доллара периодически</pre>',
            'parse_mode' => 'HTML',
            'reply_markup' => $keyboard
        ];
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return '';
    }
}