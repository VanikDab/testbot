<?php


namespace app\models;

require_once ROOT . '/classes/facades/DataValidator.php';

use app\facades\DataValidator;

/**
 * Class TelegramRequestObject
 */
class TelegramRequestObject
{
    private ?int $messageId = NULL;
    private int $userId;
    private string $firstName;
    private string $lastName;
    private string $username;
    private int $date;
    private string $command;

    /**
     * TelegramRequestObject constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        if (DataValidator::validate($data)) {
            $this->setMessageId($data['message_id']);
            $this->setUserId($data['from']['id']);
            $this->setFirstName($data['from']['first_name']);
            $this->setLastName($data['from']['last_name']);
            $this ->setUsername($data['from']['username']);
            $this->setDate($data['date']);
            $this->setCommand($data['text']);
        }
    }

    /**
     * @return int|null
     */
    public function getMessageId(): ?int
    {
        return $this->messageId;
    }

    /**
     * @param int $messageId
     * @return $this
     */
    public function setMessageId(int $messageId): self
    {
        $this->messageId = $messageId;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return int
     */
    public function getDate(): int
    {
        return $this->date;
    }

    /**
     * @param int $date
     * @return $this
     */
    public function setDate(int $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @param string $command
     * @return $this
     */
    public function setCommand(string $command): self
    {
        $this->command = mb_strtolower($command, 'utf-8');

        return $this;
    }
}