<?php


namespace app\models;

require_once ROOT . '/classes/models/Model.php';

/**
 * Class History
 * @package app\models
 *
 * Использую паттерн актив рекорд
 */
class History extends Model
{
    private ?int $id = NULL;
    private int $userId;
    private string $username;
    private string $firstName;
    private string $lastName;
    private int $date;
    private string $command;
    private string $value;

    /**
     * @return array
     */
    public function getAllUsersIds(): array
    {
        return $this->_db->query("SELECT DISTINCT userId FROM history");
    }

    /**
     * Тут в реальном проекте я бы сделал коллекцию как в ларавеле
     * @return array
     */
    public function getAll(): array
    {
        return $this->_db->query("SELECT * FROM history WHERE userId = :userId LIMIT 30", ['userId' => $this->getUserId()]);
    }

    /**
     * Save data
     */
    public function save()
    {
        $this->_db
            ->query("INSERT INTO history(userId,username,first_name,last_name,value,date,command) 
                    VALUES(:userId,:username,:first_name,:last_name,:value,:date,:command)", [
                "userId" => $this->getUserId(),
                "username" => $this->getUsername(),
                "first_name" => $this->getFirstName(),
                "last_name" => $this->getLastName(),
                "value" => $this->getValue(),
                "date" => $this->getDate(),
                "command" => $this->getCommand()
            ]);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return int
     */
    public function getDate(): int
    {
        return $this->date;
    }

    /**
     * @param int $date
     * @return $this
     */
    public function setDate(int $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @param string $command
     * @return $this
     */
    public function setCommand(string $command): self
    {
        $this->command = $command;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}